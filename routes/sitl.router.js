const express = require('express');

const router = express.Router();

/* Home page */
router.get('/status', (req, res) => {
  const sess = req.session;
  let data = {
    isLogin: false
  };
  if (sess.isLogin) {
    const { isLogin, username } = sess;
    data = {
      isLogin,
      username
    };
  }
  res.status(200).render('index', {
    page: 'home',
    title: 'XBFirm',
    data,
    message: ''
  });
});

module.exports = router;
