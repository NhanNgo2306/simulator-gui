const express = require('express');
const command = require('../controllers/commandUtils');
const Wifi = require('rpi-wifi-connection');

const router = express.Router();

/* Home page */
router.get('/client/scan', async (req, res) => {
    let i = 0;
    const response = {
    };
    while ( i <= 4) {
        try {
                const wifi = new Wifi();
                const ssids = await wifi.scan();
                res.status(200).send({
                    success: true,
                    data: ssids
                });
                return;
            } catch (e) {
                response.success = false;
                response.data = e;
                console.log("Scan error: ", e);
                i++;
            }
    }
    await command.restartDhcpcd();
    res.status(500).send(response);
});

router.post('/mode', async (req, res) => {
    try {
        const body = req.body;
        const {mode} = body;
        console.log("---> MODE", mode);
        if (mode == 'ap') {
            console.log("Disconnect wifi");
            await command.disconnectWifi();
            await command.startWifiModeAp();
            res.status(200).send({
                success: true
            });
        } else if (mode == 'client') {
            await command.startWifiModeClient();
            res.status(200).send({
                success: true
            });
        } else {
            res.status(400).send(
                {
                    success: false,
                    data: "Bad Request"
                }
            );
        }
    } catch (e) {
    console.log("Change mode error: ", e);
        res.status(500).send({
            success: false,
            data: e.toString()
        });
    }
});

router.post('/client/connect', async (req, res) => {
    try {
        const body = req.body;
        const {ssid, psk} = body;
        console.log("---> Connect", ssid, psk);
        if (ssid && psk) {
            const wifi = new Wifi();
            console.log("Connect wifi");
            await wifi.connect({ssid, psk});
            res.status(200).send({
                success: true
            });
        } else {
            res.status(400).send({
                success: false,
                data: "Bad Request"
            });
        }
    } catch (e) {
        res.status(500).send({
            success: false,
            data: e.toString()
        });
    }
});

router.get('/status', async (req, res) => {
    try {
        const wifi = new Wifi();
        const response = {};
        const isConnected = await wifi.getState();
        if (isConnected) {
            const status = await wifi.getStatus();
            console.log("--> status: ", status);
            response.ssid = status.ssid;
            response.ipAddress = status.ip_address;
        }
        response.isConnected = isConnected;
        res.status(200).send({
            success: true,
            data: response
        });
    } catch (e) {
        res.status(500).send({
            success: false,
            data: e.toString()
        });
    }
});

// router.get('/stop', async (req, res) => {
//     await command.stopSonyRemoteCli();
//     res.status(200);
// });.
module.exports = router;
