const express = require('express');
const command = require('../controllers/commandUtils');

const router = express.Router();

/* Home page */
router.get('/start', async (req, res) => {
    await command.startSonyRemoteCli();
    res.status(200);
});

router.get('/stop', async (req, res) => {
    await command.stopSonyRemoteCli();
    res.status(200);
});
module.exports = router;
