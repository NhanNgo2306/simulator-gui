const express = require('express');

const router = express.Router();

/* Home page */
router.get('/', (req, res) => {

  let responseData = {
    success: false,
    data: {

    }
  };

  if (sess.isLogin) {
    const { isLogin, username } = sess;
    data = {
      isLogin,
      username
    };
  }
  res.status(200).render('index', {
    page: 'home',
    title: 'XBFirm',
    data,
    message: ''
  });
});

module.exports = router;
