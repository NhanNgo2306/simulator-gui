const express = require('express');
const PropertiesReader = require('properties-reader');
const internalIp = require('internal-ip');
const accountController = require('../controllers/account.controller');

const router = express.Router();

/** Get account page */
router.get('/', async (req, res) => {
  const filePath = '/xbstation/XBStation.ini';
  const props = PropertiesReader(filePath);

  const sess = req.session;
  const propsUsername = props.get('General.Username');
  const networkType = props.get('General.NetworkType');
  const apnName = props.get('General.apn_name');

  const ipAddress = await internalIp.v4();

  if (propsUsername && propsUsername !== '') {
    sess.isLogin = true;
    sess.username = propsUsername;
    res.status(200).render('index', {
      page: 'account',
      title: 'XBFirm Account',
      data: {
        isLogin: true,
        username: propsUsername,
        ipAddress,
        networkType,
        apnName
      },
      message: '',
      success: true
    });
  } else {
    res.status(200).render('index', {
      page: 'account',
      title: 'XBFirm Account',
      data: {
        isLogin: false,
        ipAddress,
        networkType,
        apnName
      },
      message: '',
      success: true
    });
  }
});

/** Post sign in account */
router.post('/', accountController.login);
router.get('/start', accountController.start);
router.get('/stop', accountController.stop);

/** Logout */
router.get('/logout', accountController.logout);

module.exports = router;
