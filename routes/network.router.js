const express = require('express');

const networkController = require('../controllers/network.controller');

const router = express.Router();
const PropertiesReader = require('properties-reader');

String.prototype.replaceAll = function(search, replacement) {
  var target = this;
  return target.replace(new RegExp(search, 'g'), replacement);
};

/** Get account page */
router.get('/*', (req, res) => {
  const filePath = '/xbstation/XBStation.ini';
  const props = PropertiesReader(filePath);

  const networkType = props.get('General.NetworkType');
  let apns = props.get('General.apn_name');
  const imsi = props.get('General.last_imsi');
  const lastApn = props.get('General.last_apn');
  const apnMode = props.get('General.apn_mode');
  let apnList = undefined;
  if (apns) {
    apnList = apns.split("#");
  }
  let country = "Other";
  let network = "Other";
  let apn = "";
  if (apnList && apnList.length == 3) {
    country = apnList[0];
    network = apnList[1];
    apn = apnList[2]
  }

  country = country.replaceAll("_", " ")

  res.status(200).render('index', {
    page: 'network',
    title: 'XBFirm Network Configuration',
    data: {
      isLogin: false,
      networkType,
      apn,
      country,
      network,
      apnMode,
      imsi,
      lastApn
    },
    message: ''
  });
});

/** Post sign in account */
router.post('/type', networkController.config);
router.post('/apn', networkController.configurateAPN);
router.post('/cache', networkController.clearCache);

module.exports = router;
