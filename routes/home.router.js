const express = require('express');
const sitlController = require('../controllers/sitl.controller');
const apiController = require('../controllers/apiController');

const router = express.Router();

/* Home page */
router.get('/api/status', apiController.index);
router.post('/api/start', apiController.start);
router.post('/api/stop', apiController.stop);
router.post('/api/config', apiController.config);
router.post('/api/geo', apiController.addGeo);
router.post('/api/deleteGeo', apiController.deleteGeo);
router.post('/api/updateFlightAxis', apiController.updateFlightAxis);
router.get('/', sitlController.index);
router.post('/', sitlController.add);
router.post('/vehicle', sitlController.vehicle);
router.post('/active/:name', sitlController.active);
router.post('/delete/:name', sitlController.delete);
router.post('/start', sitlController.start);
router.post('/stop', sitlController.stop);

module.exports = router;
