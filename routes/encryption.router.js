const express = require('express');

const encryptionController = require('../controllers/encryption.controller');

const router = express.Router();

const PropertiesReader = require('properties-reader');

/** Get encryption page */
router.get('/', (req, res) => {
  const filePath = '/xbstation/XBStation.ini';
  const props = PropertiesReader(filePath);

  const algorithm = props.get('General.algorithm');
  res.status(200).render('index', {
    page: 'encryption',
    title: 'XBFirm Encryption Configuration',
    data: {
      isLogin: false,
      algorithm
    },
    message: ''
  });
});

/** Post sign in account */
router.post('/', encryptionController.config);

module.exports = router;
