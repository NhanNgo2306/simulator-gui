const express = require('express');
const modemController = require('../controllers/modem.controller');
const router = express.Router();

/** Post sign in account */
router.post('/start', modemController.start);
router.post('/stop', modemController.stop);

module.exports = router;
