const express = require('express');
const PropertiesReader = require('properties-reader');

const apnController = require('../controllers/apn.controller');
const router = express.Router();

/* Get account page */

router.get('/', (req, res) => {
  const filePath = '/xbstation/XBStation.ini';
  const props = PropertiesReader(filePath);

  const apns = props.get('General.apn_name');
  const apnList = apns.split("#");
  let country = "Other";
  let network = "Other";
  let apn = "";
  if (apnList.length == 3) {
    country = apnList[0];
    network = apnList[1];
    apn = apnList[2]
  }

  const sess = req.session;
  sess.apn = apn;
  let data = {
    isLogin: false
  };
  if (sess.isLogin) {
    const { isLogin, username } = sess;
    data = {
      isLogin,
      username,
      apn,
      network,
      country
    };
  }
  res.status(200).render('index', {
    page: 'apn',
    title: 'XBFirm APN',
    data,
    message: ''
  });
});

router.post('/', apnController.configurateAPN);

module.exports = router;
