const express = require('express');
// eslint-disable-next-line no-unused-vars
const fs = require('fs');
const versionController = require('../controllers/version.controller');

const router = express.Router();

const propertyFilePath = '/xbstation/XBStation.ini';

const PropertiesReader = require('properties-reader');

/** Get account page */
router.get('/', async (req, res) => {
  const props = PropertiesReader(propertyFilePath);

  const firmVersion = props.get('General.firm_version');
  const firms = await versionController.getVersions();
  const availables = await versionController.getAvailableVersions();
  res.status(200).render('index', {
    page: 'version',
    title: 'XBFirm Version Configuration',
    data: {
      isLogin: false,
      firms,
      availables,
      firm: firmVersion
    },
    message: ''
  });
});

router.post('/download', versionController.download);
router.post('/download/key', versionController.downloadVersion);
router.get('/available', versionController.available);
router.post('/', versionController.chooseVersion);
module.exports = router;
