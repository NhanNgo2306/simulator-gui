const command = require('./commandUtils');

String.prototype.replaceAll = function(search, replacement) {
  var target = this;
  return target.replace(new RegExp(search, 'g'), replacement);
};

exports.configurateAPN = async (req, res) => {
  console.log("body --> ", req.body)
  if (!req.body) {
    res.status(304).json({
      success: false,
      message: 'Are you kidding me?'
    });
  }

  const sess = req.session;
  let data = {
    isLogin: false
  };

  let { apnName, country, network } = req.body;
  console.log("---> vo day: ", apnName, country, network);

  data = {
    apnName,
    country,
    network
  };

  if (!apnName) {
    res.status(304).redirect("/network");
    // res.status(304).render('index', {
    //   page: 'network',
    //   title: 'XBFirm APN',
    //   data,
    //   message: 'Please input APN name',
    //   messageLevel: 'warning',
    //   success: false
    // });
  }

  if (command.apnSetting(apnName)) {
    await command.configAPNName(`${country}#${network}#${apnName}`);
    res.status(200).redirect("/network");
    // data.apn = apnName;
    // res.status(200).render('index', {
    //   page: 'network',
    //   title: 'XBFirm Account',
    //   data,
    //   message: 'Successfully Configuration!',
    //   messageLevel: 'success',
    //   success: true
    // });
  } else {
    res.status(401).redirect("/network");
    // res.status(401).render('index', {
    //   page: 'network',
    //   title: 'XBFirm Account',
    //   data,
    //   message: 'Failed Configuration!',
    //   messageLevel: 'warning',
    //   success: true
    // });
  }
};
