const fs = require('fs');
const request = require('request');
const extract = require('extract-zip');
const Axios = require('axios');
const command = require('./commandUtils');

// eslint-disable-next-line no-unused-vars
const directoryPath = '/home/pi/XBStationManagerHeadless/XBFirms';

exports.getAvailableVersions = async () => {
  const availableFirms = [];
  fs.readdirSync(directoryPath).forEach(file => {
    if (file.startsWith('XBFirm-')) {
      availableFirms.push(file);
    }
  });
  return availableFirms;
}

const unzip = async (file, desc) => {
  try {
    await extract(file, {
      dir: desc
    });
  } catch (err) {
    // handle any errors
  }
}

const removeFile = async file => {
  try {
    fs.unlinkSync(file);
  } catch (err) {
    console.error(err);
  }
}

const download = async (url, dest) => {
  const file = fs.createWriteStream(dest);
  const response = await Axios({
    url,
    method: 'GET',
    responseType: 'stream'
  });
  response.data.pipe(file);
  return new Promise((resolve, reject) => {
    file.on('finish', resolve);
    file.on('error', reject);
  });
};

const doRequest = url => {
  return new Promise((resolve, reject) => {
    request(url, (error, res, body) => {
      if (!error && res.statusCode === 200) {
        resolve(body);
      } else {
        reject(error);
      }
    });
  });
}

exports.downloadVersion = async (req, res) => {
  const {key} = req.body;
  const response = {
    success: false
  }
  if (key) {
    try {
      const url = `http://xbstation.com/api/v1/firms/${key}`
      const dest = `${directoryPath}/temp.zip`;
      const data = await doRequest(url);
      const jsonData = JSON.parse(data);
      if (data && jsonData.data) {
        await download(jsonData.data, dest);
        await unzip(dest, directoryPath);
        await removeFile(dest);
        response.success = true;
      }
    } catch (e) {
      response.success = false;
    }
  }
  return res.status(200).json(response);
}

exports.download = async (req, res) => {
  const {url, name} = req.body;
  const response = {
    success: false
  }
  if (url && name) {
    const dest = `${directoryPath}/${name}.zip`;
    try {
      await download(url, dest);
      await unzip(dest, directoryPath);
      await removeFile(dest);
      response.success = true;
    } catch (e) {
      response.success = false;
    }
  }
  return res.status(200).json(response);
}

// eslint-disable-next-line no-unused-vars
exports.getVersions = async (req, res) => {
  const data = await doRequest('http://xbstation.com/api/v1/firms');
  return JSON.parse(data);
};

exports.available = async (req, res) => {
  const response = {
    success: true,
    data: []
  }
  response.data = await this.getAvailableVersions();
  return res.status(200).json(response);
}

exports.chooseVersion = async (req, res) => {
  const {firm} = req.body;
  const fullFilePath = `${directoryPath}/${firm}`;
  const firms = await this.getVersions(req, res)
  const availables = await this.getAvailableVersions();
  const sess = req.session;
  try {
    if (fs.existsSync(fullFilePath)) {
      const shFilePath = `${directoryPath}/XBFirm.sh`;
      await removeFile(shFilePath);
      const shContent = `#!/bin/bash\nsudo /etc/init.d/XBppp init\nsudo chmod +x -R /home/pi/XBStationManagerHeadless/XBFirms/${firm}\nsudo LD_LIBRARY_PATH=/home/pi/XBStationManagerHeadless/XBFirms/${firm}/libs LD_PRELOAD=/usr/lib/arm-linux-gnueabihf/libv4l/v4l1compat.so /home/pi/XBStationManagerHeadless/XBFirms/${firm}/XBFirm run "$@" --delay 1`;
      await fs.writeFileSync(shFilePath, shContent);
      await command.providePermission(shFilePath);
    }
    sess.firm = firm;
    await command.configFirmVersion(firm);
    res.status(200).render('index', {
      page: 'version',
      title: 'Version Config',
      data: {
        firms,
        availables,
        firm: sess.firm
      },
      message: 'Success!',
      messageLevel: 'success',
      success: true
    });
  } catch (err) {
    res.status(401).render('index', {
      page: 'version',
      title: 'Version Configuration',
      data: {
        firms,
        availables,
        firm: sess.firm
      },
      message: 'Please try again!',
      messageLevel: 'warning',
      success: false
    });
  }
}