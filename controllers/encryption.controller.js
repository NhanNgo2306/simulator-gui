const axios = require('axios');
const command = require('./commandUtils');

exports.config = (req, res) => {
  if (!req.body) {
    res.status(304).json({
      success: false,
      message: 'Are you kidding me?'
    });
  }

  // console.log(req.body);
  const { algorithm } = req.body;
  if (command.configEncryptionAlgorithm(algorithm)) {
    const sess = req.session;
    sess.algorithm = algorithm;
    // res.status(200).redirect('/');
    res.status(200).render('index', {
      page: 'encryption',
      title: 'Encryption',
      data: {
        algorithm
      },
      message: 'Success!',
      messageLevel: 'success',
      success: true
    });
  } else {
    // res.status(401).json({
    //   message: 'Please try again 1',
    //   messageLevel: 'warning',
    //   success: false
    // });
    res.status(401).render('index', {
      page: 'encryption',
      title: 'XBFirm Encryption Configuration',
      data: {
        isLogin: false
      },
      message: 'Please try again!',
      messageLevel: 'warning',
      success: false
    });
  }
};
