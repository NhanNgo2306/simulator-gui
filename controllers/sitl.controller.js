const axios = require('axios');
const internalIp = require('internal-ip');
const command = require('./commandUtils');
const PropertiesReader = require('properties-reader');
const filePath = '/etc/xbstation/sitl.ini';
let props = PropertiesReader(filePath, {writer: { saveSections: true }});

exports.changeSITLCommand = () => {
  const name = this.getValue('General.currentGeoName');
  let geosData = this.getValue('General.geos');
  const geos = JSON.parse(geosData);
  const geo = geos[name];
  let vehicle = this.getValue('General.currentVehicle');
  const enableFlightAxis = this.getValue("General.enableFlightAxis");
  const flightAxisIpAddress = this.getValue("General.flightAxisIpAddress");
  if (!vehicle) {
    vehicle = "ArduCopter";
  }
  if (geo && vehicle) {
    const { lat, lng } = geo;
    command.setNewLocationCommand(lat, lng, vehicle, enableFlightAxis, flightAxisIpAddress);
  }
}

exports.addValue = (key, value) => {
  props.set(key, value);
  props.save(filePath);
}

exports.getValue = (key) => {
  return props.get(key);
}

exports.index = async (req, res) => {
  props = PropertiesReader(filePath, {writer: { saveSections: true }});
  const isSITLActived = await command.isSITLActived();
  const sess = req.session;
  sess.isSITLActived = isSITLActived;
  const geoData = this.getValue('General.geos');
  let geos = [];
  if (geoData) {
    const geoDataObject = JSON.parse(geoData);
    const keys = Object.keys(geoDataObject);
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i];
      const geoObj = geoDataObject[key];
      geos.push(geoObj);
    }
  }
  const currentGeoName = this.getValue("General.currentGeoName");
  const currentVehicle = this.getValue("General.currentVehicle");
  const enableFlightAxis = this.getValue("General.enableFlightAxis");
  const flightAxisIpAddress = this.getValue("General.flightAxisIpAddress");

  res.status(200).render('index', {
    page: 'SITL',
    title: 'SITL',
    data: {
      isSITLActived,
      geos,
      currentGeoName,
      currentVehicle,
      enableFlightAxis: enableFlightAxis ? enableFlightAxis : 0,
      flightAxisIpAddress: flightAxisIpAddress ? flightAxisIpAddress : '127.0.0.1'
    },
    success: true
  });
};

exports.add = (req, res) => {
  const { name, lat, lng, desc } = req.body;
  let geosData = this.getValue('General.geos');

  let data = {};
  if (geosData) {
    data = JSON.parse(geosData);
  }
  data[name] = {
    name, lat, lng, desc
  };
  this.addValue('General.geos', JSON.stringify(data));
  res.redirect('/');
};

exports.delete = (req, res) => {
  const { name } = req.params;
  let geosData = this.getValue('General.geos');
  const data = JSON.parse(geosData);
  data[name] = undefined;
  this.addValue('General.geos', JSON.stringify(data));
  res.redirect('/');
};

exports.updateFlightAxis = (req, res) => {
  // const name = this.getValue('General.currentGeoName');
  // let geosData = this.getValue('General.geos');
  // const geos = JSON.parse(geosData);
  // const geo = geos[name];
  // let vehicle = this.getValue('General.currentVehicle');
  // if (!vehicle) {
  //   vehicle = "ArduCopter";
  // }
  // if (geo && vehicle) {
  //   const { lat, lng } = geo;
  //   command.setNewLocationCommand(lat, lng, vehicle);
  // }

  const { enableFlightAxis, ipAddress } = req.params;
  if (enableFlightAxis) {
    command.enableFlightAxis(ipAddress);
  } else {
    command.disableFlightAxis();
  }
  this.addValue('General.enableFlightAxis', enableFlightAxis);
  this.addValue('General.flightAxisIpAddress', ipAddress);

  res.redirect('/');
};

exports.start = (req, res) => {
  command.startSITL();
  res.redirect('/');
};

exports.stop = (req, res) => {
  command.stopSITL();
  res.redirect('/');
};

exports.active = (req, res) => {
  const { name } = req.params;
  this.addValue("General.currentGeoName", name);
  this.changeSITLCommand();
  res.redirect('/');
}

exports.vehicle = (req, res) => {
  const { vehicle } = req.body;
  this.addValue('General.currentVehicle', vehicle);
  this.changeSITLCommand();
  res.redirect('/');
};
