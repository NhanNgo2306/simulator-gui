const shell = require('shelljs');

//
const runCommand = async (commandLine) => {
  // show command in terminal
  console.log(commandLine);
  try {
    // running command
    const shellResult = await shell.exec(commandLine);
    const shellResultNumber = parseInt(shellResult.stdout);
    if (shellResultNumber) {
      return true;
    } else {
      return false;
    }
  } catch (e) {
    console.log("--> Run command exception: ", e);
    return false;
  }
  return true;
}

exports.login = loginData => {
  const commandLine = `sudo /home/pi/XBStationManagerHeadless/XBStationManagerHeadless config \
--host ${loginData.host} \
--host_auth ${loginData.host_auth} \
--port_controlling ${loginData.port_controlling} \
--port_socket ${loginData.port_socket} \
--port_streaming ${loginData.port_streaming} \
--port_camera ${loginData.port_camera} \
--api_info_url ${loginData.api_info_url} \
--api_get_tracking_token ${loginData.api_get_tracking_token} \
--username ${loginData.username} \
--token ${loginData.token} \
--host_terminal ${loginData.host_terminal} \
--host_auth_terminal ${loginData.host_auth_terminal} \
--api_info_terminal_url ${loginData.api_info_terminal_url} `;

  return runCommand(commandLine);
};

exports.logout = async () => {
  const commandLine = `sudo /home/pi/XBStationManagerHeadless/XBStationManagerHeadless logout`;
  return runCommand(commandLine);
};
exports.apnSetting = async (apnValue) => {
  const commandLine = `sudo /home/pi/apn/install.sh ${apnValue}`;
  return runCommand(commandLine);
};

exports.updateVersion = async (version) => {
  console.log(version);
};

exports.configNetworkType = async (networkType) => {
  // const commandLine = `sudo ~/XBStationManagerHeadless/XBStationManagerHeadless config --networkType ${networkType}`;
  const commandLine = `sudo /home/pi/XBStationManagerHeadless/XBStationManagerHeadless config --networkType ${networkType}`;

  return runCommand(commandLine);
}

exports.configEncryptionAlgorithm = async (algorithm) => {
  const commandLine = `sudo /home/pi/XBStationManagerHeadless/XBStationManagerHeadless config --encryptionAlgorithm ${algorithm}`;

  return runCommand(commandLine);
}

exports.configFirmVersion = async (version) => {
  const commandLine = `sudo /home/pi/XBStationManagerHeadless/XBStationManagerHeadless config --firmVersion ${version}`;
  return runCommand(commandLine);
}

exports.configAPNName = async (version) => {
  const commandLine = `sudo /home/pi/XBStationManagerHeadless/XBStationManagerHeadless config --apnName ${version}`;
  return runCommand(commandLine);
}

exports.configAPNMode = async (mode) => {
  const commandLine = `sudo /home/pi/XBStationManagerHeadless/XBStationManagerHeadless config --apnMode ${mode}`;
  return runCommand(commandLine);
}

exports.startXBFirm = async () => {
  const commandLine = `sudo snap start xbfirm.run --enable`;
  return runCommand(commandLine);
}

exports.stopXBFirm = async () => {
  const commandLine = `sudo snap stop xbfirm.run --disable`;
  return runCommand(commandLine);
}

exports.providePermission = async (xbfirmPath) => {
  const commandLine = `sudo chmod +x ${xbfirmPath}`;
  return runCommand(commandLine);
}

exports.clearCache = async () => {
  const commandLine = `sudo /home/pi/XBStationManagerHeadless/XBStationManagerHeadless config --imsi 0`;
  return runCommand(commandLine);
}

exports.isSITLActived = async () => {
  const commandLine = `sudo systemctl is-active --quiet sitl.service && echo 1`;
  return runCommand(commandLine);
}

exports.startSITL = async () => {
  const commandLine = `sudo systemctl enable sitl.service`;
  await runCommand(commandLine);
  const commandLine2 = `sudo systemctl start sitl.service`;
  return runCommand(commandLine2);
}

exports.startModem = async (apn, username, password) => {
  let commandLine = `sudo snap start xbfirm.modem`;
  return runCommand(commandLine);
}

exports.stopModem = async () => {
  const commandLine = `sudo snap stop xbfirm.modem`;
  return runCommand(commandLine);
}

exports.stopSITL = async () => {
  const commandLine = `sudo systemctl disable sitl.service`;
  await runCommand(commandLine);
  const commandLine2 = `sudo systemctl stop sitl.service`;
  return runCommand(commandLine2);
}

exports.setNewLocationCommand = async (lat, lng, vehicle, enableFlightAxis, ipAddress) => {
  let script;
  if (enableFlightAxis) {
    script = `#!/bin/bash
  cd /home/pi/ardupilot/${vehicle} && ../Tools/autotest/sim_vehicle.py --no-mavproxy --custom-location=${lat},${lng},0,0 -f flightaxis:${ipAddress}`
  } else {
    script = `#!/bin/bash
  cd /home/pi/ardupilot/${vehicle} && ../Tools/autotest/sim_vehicle.py --no-mavproxy --custom-location=${lat},${lng},0,0`
  }
  await runCommand(`sudo rm -rf /usr/local/bin/sitl.sh`);
  await runCommand(`sudo echo "${script}" >> /usr/local/bin/sitl.sh << EOF`);
  await runCommand(`sudo chmod +x /usr/local/bin/sitl.sh`);

  await runCommand(`sudo rm -rf /usr/bin/sitl.sh`);
  await runCommand(`sudo echo "${script}" >> /usr/bin/sitl.sh << EOF`);
  await runCommand(`sudo chmod +x /usr/bin/sitl.sh`);
}

// exports.setNewLocationCommand = async (lat, lng, vehicle, enableFlightAxis, ipAddress) => {
//   let script;
//   if (enableFlightAxis) {
//     script = `#!/bin/bash
//   /home/pi/ardupilot/Tools/autotest/sim_vehicle.py --no-mavproxy -v ${vehicle} --custom-location=${lat},${lng},0,0 -f flightaxis:${ipAddress}`
//   } else {
//     script = `#!/bin/bash
//   /home/pi/ardupilot/Tools/autotest/sim_vehicle.py --no-mavproxy -v ${vehicle} --custom-location=${lat},${lng},0,0`
//   }
//   // const script = `#!/bin/bash
//   // /home/pi/arodeldupilot/Tools/autotest/sim_vehicle.py --no-mavproxy -v ${vehicle} --custom-location=${lat},${lng},0,0`
//   await runCommand(`sudo rm -rf /usr/local/bin/sitl.sh`);
//   await runCommand(`sudo echo "${script}" >> /usr/local/bin/sitl.sh << EOF`);
//   await runCommand(`sudo chmod +x /usr/local/bin/sitl.sh`);
// }

exports.disableFlightAxis = async (lat, lng, vehicle) => {
  const script = `#!/bin/bash
  cd /home/pi/ardupilot/${vehicle} && ../Tools/autotest/sim_vehicle.py --no-mavproxy --custom-location=${lat},${lng},0,0`
  await runCommand(`sudo rm -rf /usr/local/bin/sitl.sh`);
  await runCommand(`sudo echo "${script}" >> /usr/local/bin/sitl.sh << EOF`);
  await runCommand(`sudo chmod +x /usr/local/bin/sitl.sh`);

  await runCommand(`sudo rm -rf /usr/bin/sitl.sh`);
  await runCommand(`sudo echo "${script}" >> /usr/bin/sitl.sh << EOF`);
  await runCommand(`sudo chmod +x /usr/bin/sitl.sh`);
}

exports.enableFlightAxis = async (ipAddress, lat, lng, vehicle) => {
  const script = `#!/bin/bash
  cd /home/pi/ardupilot/${vehicle} && ../Tools/autotest/sim_vehicle.py --no-mavproxy --custom-location=${lat},${lng},0,0 -f flightaxis:${ipAddress}`
  await runCommand(`sudo rm -rf /usr/local/bin/sitl.sh`);
  await runCommand(`sudo echo "${script}" >> /usr/local/bin/sitl.sh << EOF`);
  await runCommand(`sudo chmod +x /usr/local/bin/sitl.sh`);

  await runCommand(`sudo rm -rf /usr/bin/sitl.sh`);
  await runCommand(`sudo echo "${script}" >> /usr/bin/sitl.sh << EOF`);
  await runCommand(`sudo chmod +x /usr/bin/sitl.sh`);
}

exports.startSonyRemoteCli = () => {
  const commandLine = `sudo systemctl start remoteCli.service`;
  return runCommand(commandLine);
};

exports.stopSonyRemoteCli = async () => {
  const commandLine = `sudo systemctl stop remoteCli.service`;
  return runCommand(commandLine);
};

exports.startWifiModeAp = async () => {
  const dhcpcdScript = `hostname\nclientid\n\persistent\noption rapid_commit\noption domain_name_servers 8.8.8.8 8.8.4.4, domain_name, domain_search, host_name\noption classless_static_routes\noption ntp_servers\nrequire dhcp_server_identifier\nslaac private\nnohook lookup-hostname\n# RaspAP wlan0 configuration\ninterface wlan0\nstatic ip_address=10.0.0.1/24\nstatic routers=10.0.0.1\ndenyinterfaces wwan0\ndenyinterfaces wwan0`;
  await runCommand(`rm -rf /etc/dhcpcd.conf`);
  await runCommand(`sudo echo "${dhcpcdScript}" >> /etc/dhcpcd.conf << EOF`);
  await runCommand(`sudo systemctl restart dhcpcd`);
  try {
      await runCommand(`sudo busybox udhcpc -f -n -q -t 1 -i wwan0 -s /var/snap/xbfirm/common/custom.script`);
    } catch (e) {
    console.log("Error with busy box", e);
    console.log("Reset busy box", e);
  }
  const commandLine = `sudo systemctl stop wpa_supplicant.service && sudo systemctl start hostapd.service`;
  return runCommand(commandLine);
};

exports.disconnectWifi = async () => {
  const commandLine = `sudo wpa_cli disconnect`;
  return runCommand(commandLine);
};

exports.restartDhcpcd = async () => {
  const commandLine = `sudo systemctl restart dhcpcd`;
  return runCommand(commandLine);
};

exports.startWifiModeClient = async () => {
  const dhcpcdScript = `hostname\nclientid\n\persistent\noption rapid_commit\noption domain_name_servers, domain_name, domain_search, host_name\noption classless_static_routes\noption ntp_servers\nrequire dhcp_server_identifier\nslaac private\nnohook lookup-hostname\ndenyinterfaces wwan0\ndenyinterfaces wwan0`;
  await runCommand(`rm -rf /etc/dhcpcd.conf`);
  await runCommand(`sudo echo "${dhcpcdScript}" >> /etc/dhcpcd.conf << EOF`);
  await runCommand(`sudo systemctl restart dhcpcd`);
//  await runCommand(`sudo systemctl restart dhcpcd`);
    try {
    await runCommand(`sudo busybox udhcpc -f -n -q -t 1 -i wwan0 -s /var/snap/xbfirm/common/custom.script`);
    console.log("Reset busy box", e);
  } catch (e) {
    console.log("Error with busy box", e);
  }
  const commandLine = `sudo systemctl stop hostapd.service && sudo systemctl start wpa_supplicant.service`;
  return runCommand(commandLine);

};