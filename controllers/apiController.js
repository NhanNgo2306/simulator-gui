const axios = require('axios');
const internalIp = require('internal-ip');
const command = require('./commandUtils');
const PropertiesReader = require('properties-reader');
const filePath = '/etc/xbstation/sitl.ini';
const props = PropertiesReader(filePath, {writer: { saveSections: true }});

exports.getValue = (key) => {
  return props.get(key);
}

exports.addValue = (key, value) => {
  props.set(key, value);
  props.save(filePath);
}


exports.index = async (req, res) => {
  const isSITLActivated = await command.isSITLActived();
  let geoData = this.getValue('General.geos');
  let geos = [];
  if (geoData) {
    try {
      const geoDataObject = JSON.parse(geoData);
      const keys = Object.keys(geoDataObject);
      for (let i = 0; i < keys.length; i++) {
        const key = keys[i];
        const geoObj = geoDataObject[key];
        geos.push(geoObj);
      }
    } catch (e) {
      geoData = undefined;
    }
  }
  const currentGeoName = this.getValue("General.currentGeoName");
  const currentVehicle = this.getValue("General.currentVehicle");
  res.status(200).send({
    success: true,
    data: {
      isRunning: isSITLActivated,
      geos,
      currentGeoName,
      currentVehicle
    }
  });
}

exports.configVehicleType = async (req, res) => {
  const { currentVehicle } = req.body;
  this.addValue('General.currentVehicle', currentVehicle);
  this.index(req, res);
}

exports.config = async (req, res) => {
  const { currentVehicle, currentGeoName } = req.body;
  if (currentVehicle) {
    this.addValue('General.currentVehicle', currentVehicle);
  }

  if (currentGeoName) {
    this.addValue('General.currentGeoName', currentGeoName);
  }
  this.index(req, res);
}

exports.addGeo = async (req, res) => {
  const { name, lat, lng, desc } = req.body;
  let geosData = this.getValue('General.geos');
  let data = {};
  if (geosData) {
    try {
      data = JSON.parse(geosData);
    } catch (e) {
      data = {};
    }
  }
  data[name] = {
    name, lat, lng, desc
  };
  this.addValue('General.geos', JSON.stringify(data));
  this.index(req, res);
}

exports.deleteGeo = async (req, res) => {
  const { name } = req.body;
  let geosData = this.getValue('General.geos');
  const data = JSON.parse(geosData);
  data[name] = undefined;
  this.addValue('General.geos', JSON.stringify(data));
  this.index(req, res);
}

exports.start = async (req, res) => {
  await command.startSITL();
  const isSITLActivated = await command.isSITLActived();
  res.status(200).send({
    success: true,
    data: {
      isRunning: isSITLActivated,
    }
  });
};

exports.stop = async (req, res) => {
  await command.stopSITL();
  const isSITLActivated = await command.isSITLActived();
  res.status(200).send({
    success: true,
    data: {
      isRunning: isSITLActivated,
    }
  });
};

exports.updateFlightAxis = async (req, res) => {
  const name = this.getValue('General.currentGeoName');
  let geosData = this.getValue('General.geos');
  const geos = JSON.parse(geosData);
  const geo = geos[name];
  let vehicle = this.getValue('General.currentVehicle');
  if (!vehicle) {
    vehicle = "ArduCopter";
  }
  if (geo && vehicle) {
    const { lat, lng } = geo;
    const { enableFlightAxis, flightAxisIpAddress } = req.body;
    if (enableFlightAxis == 1) {
      await command.enableFlightAxis(flightAxisIpAddress, lat, lng, vehicle);
    } else {
      await command.disableFlightAxis(lat, lng, vehicle);
    }
    this.addValue('General.enableFlightAxis', enableFlightAxis == 1 ? 1 : 0);
    this.addValue('General.flightAxisIpAddress', flightAxisIpAddress);
    res.status(200).send({
      success: true,
    });
  } else {
    res.status(200).send({
      success: false,
    });
  }
  // res.redirect('/');
};
