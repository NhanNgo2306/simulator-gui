const command = require('./commandUtils');

exports.start = async (req, res) => {
  try {
    await command.startModem();
    res.status(200).send({
      success: true
    });
  } catch (e) {
    console.log("--> error: ", e);
    res.status(500).send({
      success: false
    });
  }
};

exports.stop = async (req, res) => {
  try {
    await command.stopModem();
    res.status(200).send({
      success: true
    });
  } catch (e) {
    console.log("--> error: ", e);
    res.status(500).send({
      success: true
    });
  }
};

