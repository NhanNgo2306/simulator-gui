const axios = require('axios');
const command = require('./commandUtils');

String.prototype.replaceAll = function (search, replacement) {
  var target = this;
  return target.replace(new RegExp(search, 'g'), replacement);
};

exports.config = (req, res) => {
  if (!req.body) {
    res.status(304).redirect("/network");
  }

  const {networkType} = req.body;
  if (command.configNetworkType(networkType)) {
    const sess = req.session;
    sess.networkType = networkType;
    res.status(200).redirect("/network");
  } else {
    res.status(401).redirect("/network");
  }
};

exports.clearCache = (req, res) => {
  const {networkType} = req.body;
  if (command.clearCache()) {
    res.status(200).redirect("/network");
  } else {
    res.status(401).redirect("/network");
  }
};

exports.configurateAPN = async (req, res) => {
  console.log("body --> ", req.body)
  if (!req.body) {
    res.status(304).json({
      success: false,
      message: 'Are you kidding me?'
    });
  }

  const sess = req.session;
  let data = {
    isLogin: false
  };

  let {apnName, country, network, apnMode} = req.body;
  if (apnMode == "1") {
    country = country.replaceAll(" ", "_");
    data = {
      apnName,
      country,
      network
    };
    if (!apnName) {
      res.status(304).redirect("/network");
    }
    await command.configAPNMode(apnMode);
    if (command.apnSetting(apnName)) {
      await command.configAPNName(`${country}#${network}#${apnName}`);
      data.apn = apnName;
      res.status(200).redirect("/network");
    } else {
      res.status(401).redirect("/network");
    }
  } else {
    console.log("AUTO mode");
    await command.configAPNMode(apnMode);
    res.status(200).redirect("/network");
  }
};

