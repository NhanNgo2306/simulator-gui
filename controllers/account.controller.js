const axios = require('axios');
const internalIp = require('internal-ip');
const command = require('./commandUtils');
const PropertiesReader = require('properties-reader');

exports.start = async (req, res) => {
  const ipAddress = await internalIp.v4();

  try {
    await command.startXBFirm();
    res.status(200).redirect('/account');
  } catch (e) {
    res.status(200).redirect('/account');
  }
}

exports.stop = async (req, res) => {
  const ipAddress = await internalIp.v4();

  try {
    await command.stopXBFirm();
    res.status(200).redirect('/account');
  } catch (e) {
    res.status(200).redirect('/account');
  }
}

exports.login = async (req, res) => {
  if (!req.body) {
    res.status(304).json({
      success: false,
      message: 'Are you kidding me?'
    });
  }

  const filePath = '/xbstation/XBStation.ini';
  const props = PropertiesReader(filePath);

  const apnName = props.get('General.apn_name');
  const networkType = props.get('General.NetworkType');
  const {username, password, server} = req.body;
  const ipAddress = await internalIp.v4();

  if (!(username && password && server)) {
    res.status(304).render('index', {
      page: 'account',
      title: 'XBFirm Account',
      data: {
        isLogin: false,
        ipAddress,
        networkType,
        apnName
      },
      message: 'Please input username, password and server',
      messageLevel: 'warning',
      success: false
    });
  } else {
    axios
      .post(`${server}/api/firm/getToken`, {username, password})
      .then(response => {
        if (!response.data) {
          console.log('login failed');
          // 401 is the proper response code to send when a failed login has happened.
          res.status(401).render('index', {
            page: 'account',
            title: 'XBFirm Account',
            data: {
              isLogin: false,
              ipAddress,
              networkType,
              apnName
            },
            message: 'Please try again 1',
            messageLevel: 'warning',
            success: false
          });
        }

        const {token} = response.data.data;
        response.data.data.info_server.token = token;
        response.data.data.info_server.username = username;
        if (command.login(response.data.data.info_server)) {
          // if (true) {
          // res.status(200).json({
          //   success: 'OK'
          // });
          const sess = req.session;
          sess.isLogin = true;
          sess.username = username;
          sess.server = server;
          sess.token = token;
          // res.status(200).redirect('/');
          res.status(200).render('index', {
            page: 'account',
            title: 'XBFirm Account',
            data: {
              isLogin: true,
              username,
              ipAddress,
              networkType,
              apnName
            },
            message: 'Signed in successfully!',
            messageLevel: 'success',
            success: true
          });
        } else {
          res.status(401).render('index', {
            page: 'account',
            title: 'XBFirm Account',
            data: {
              isLogin: false,
              ipAddress,
              networkType,
              apnName
            },
            message: 'Please try again!',
            messageLevel: 'warning',
            success: false
          });
        }
      })
      .catch(err => {
        res.status(401).render('index', {
          page: 'account',
          title: 'XBFirm Account',
          data: {
            isLogin: false,
            ipAddress,
            networkType,
            apnName
          },
          message: 'Log in Failed!',
          messageLevel: 'warning',
          success: false
        });
      });
  }
};

exports.logout = (req, res) => {
  if (req.session && req.session.isLogin === true) {
    if (command.logout()) {
      req.session.destroy();
    }
  }
  res.status(200).redirect('/account');
};
