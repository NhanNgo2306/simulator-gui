const express = require('express');
const session = require('express-session');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const homeRouter = require('./routes/home.router');
const modemRouter = require('./routes/modem.router');
const sonyRouter = require('./routes/sony.router');
const accountRouter = require('./routes/account.router');
const sitlRouter = require('./routes/sitl.router');
const wifiRouter = require('./routes/wifi.router');

const app = express();
const PORT = 3002;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({secret: 'ssshhhhh', saveUninitialized: true, resave: true}));

// Configuration
// const Config = require('./configs');

// console.log('Configuration', Config);

// Home
app.use('/', homeRouter);
app.use('/modem', modemRouter);
app.use('/sony', sonyRouter);
app.use('/wifi', wifiRouter);

app.listen(PORT, err => {
  if (err) {
    console.log(err);
  }
  console.log(`Server is listening on ${PORT}`);
});
