const shell = require('shelljs');

const runCommand = async (commandLine) => {
  // show command in terminal
  console.log(commandLine);
  try {
    // running command
    const shellResult = await shell.exec(commandLine);
    const shellResultNumber = parseInt(shellResult.stdout);
    if (shellResultNumber) {
      return true;
    } else {
      return false;
    }
  } catch (e) {
    console.log("--> Run command exception: ", e);
    return false;
  }
  return true;
}

const start = async () => {
  const dhcpcdScript = `hostname\nclientid\n\persistent\noption rapid_commit\noption domain_name_servers, domain_name, domain_search, host_name\noption classless_static_routes\noption ntp_servers\nrequire dhcp_server_identifier\nslaac private\nnohook lookup-hostname\n# RaspAP wlan0 configuration\ninterface wlan0\nstatic ip_address=10.0.0.1/24\nstatic routers=10.0.0.1\ndenyinterfaces wwan0\ndenyinterfaces wwan0`;
  await runCommand(`rm -rf /etc/dhcpcd.conf`);
  await runCommand(`sudo echo "${dhcpcdScript}" >> /etc/dhcpcd.conf << EOF`);
  await runCommand(`sudo systemctl restart dhcpcd`);
  const commandLine = `sudo systemctl stop wpa_supplicant.service && sudo systemctl start hostapd.service`;
  await runCommand(commandLine);
  process.exit(0);
};



start();
// process.exit(1);
